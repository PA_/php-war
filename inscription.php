
<h1> Création de compte PHP WAR </h1> 
<form action="inscription.php" method="post">
    <p> Identifiant/pseudo : <input type="text" name="identifiant" value="" />  </p>
    <p> Mot de passe : <input type="password" name="password" value="" />  </p>
    <p> Confirmation mot de passe : <input type="password" name="conf_password" value="" />  </p>
    <p> Modele de char disponible : <select name="choix_modele">
    	<option value="M1-Abrams"> M1-Abrams </option>
    	<option value="Leclerc"> Leclerc </option>
		<option value="Challenge_2"> Challenge_2 </option>
		<option value="T-90"> T-90 </option>
		<option value="Merkava"> Merkava </option>
		<option value="Leopard_2"> Leopard_2 </option>
	</select>
	</br> </br>
    <input type="submit" name="inscription" value="Confirmation" />
    <input type="submit" name="annuler" value="Annuler" />
</form>

<?php

// Si on s'inscrit
if(isset($_POST['inscription'])) {
    $identifiant = ($_POST['identifiant']) ;
    $password = ($_POST['password']) ;
    $conf_password = ($_POST['conf_password']) ;
    $modele = ($_POST['choix_modele']) ;

	try{
 		$pdo = new PDO('pgsql:host=localhost ; port=5432 ; user=pa ; dbname=php_war ;password=root');
	}catch (PDOException $e){
		echo $e->getMessage("erreur connection");
	}

	// Check si l'identifiant entré existe 
	$connexion= $pdo->prepare("SELECT count(*) from utilisateur WHERE identifiant = :identifiant ") ;
    $connexion->bindParam(":identifiant",$identifiant) ;
    $connexion->execute() ;
    $resultat = $connexion->fetchColumn() ;

    // Test d'erreur dans le formulaire
    if($resultat == 1) {
        echo "L'identifiant existe déjà ";
    } else if ($password != $conf_password){
    	echo "Veuillez remplir le formulaire svp" ;
    } else if ($identifiant == "" && $password == "" && $conf_password == "" ) {
    	echo "Mots de passe différents" ;
    } else {
    	echo "C'est bon" ;
    	$password_cry = hash('ripemd160',"$password") ; // Voir pour changer le hachage

    	// Création de l'utilisateur
        $insert= $pdo->prepare("INSERT INTO utilisateur(pseudo,password) VALUES (:pseudo,:password)") ;    	
		$insert->bindParam(":pseudo",$identifiant) ;
		$insert->bindParam(":password",$password_cry) ;
		$insert->execute() ;
		echo "<br>" ; 
		echo "$password_cry" ;

		// Selection du l'id_user pour le lier avec son nouveau vehicule
		$select= $pdo->prepare("SELECT id_user FROM utilisateur WHERE pseudo LIKE :pseudo") ;
		$select->bindParam(":pseudo",$identifiant) ;
		$select->execute() ;
		$res = $select->fetchAll();
		$id_user = (int) $res[0]["id_user"] ;

        // Recherche statistiques par défaut selon le modèle du char choisi par le joueur
        $creation= $pdo->prepare("SELECT attaque,portee,blindage,vitesse FROM char WHERE modele LIKE :modele") ;
		$creation->bindParam(":modele",$modele) ;
		$creation->execute() ;
		$res = $creation->fetchAll();

		// Statistiques par défaut du char
		$type = "char" ; 
		$attaque = (float) $res[0]["attaque"] ;
		$portee = (float) $res[0]["portee"] ;		
		$blindage = (float) $res[0]["blindage"] ;		
		$vitesse = (float) $res[0]["vitesse"] ;

		// Création du char dans la table vehicule
        $insert= $pdo->prepare("INSERT INTO vehicule (id_user,type,modele,attaque,portee,blindage,vitesse) 
       	VALUES (:id_user,:type,:modele,:attaque,:portee,:blindage,:vitesse)") ;
 		$insert->bindParam(":id_user",$id_user) ;
 		$insert->bindParam(":type",$type) ;
 		$insert->bindParam(":modele",$modele) ;
 		$insert->bindParam(":attaque",$attaque) ;
 		$insert->bindParam(":portee",$portee) ;
 		$insert->bindParam(":blindage",$blindage) ;
 		$insert->bindParam(":vitesse",$vitesse) ;
 		$insert->execute() ;
 		header('Location: http://pwcs.local');
  		exit();
 	}
}

// Si on annule l'inscription
if(isset($_POST['annuler'])) {
	header('Location: http://pwcs.local');
	exit();
}

?>