<?php

class Vehicule {

	protected $modele ;
	protected $vie ;
	protected $attaque ;
	protected $portee ;
	protected $blindage ;
	protected $vitesse ;

	// Constructeur
	public function __construct(string $modele) {
		try{
 			$pdo = new PDO('pgsql:host=localhost ; port=5432 ; user=pa ; dbname=php_war ;password=root');
		}catch (PDOException $e){
			echo $e->getMessage("erreur connection");
		}

		$creation= $pdo->prepare("SELECT attaque,portee,blindage,vitesse FROM char WHERE modele LIKE :modele") ;
		$creation->bindParam(":modele",$modele) ;
		$creation->execute() ;
		$res = $creation->fetchAll();
		$attaque = (float) $res[0]["attaque"] ;
		$portee = (float) $res[0]["portee"] ;		
		$blindage = (float) $res[0]["blindage"] ;
		$vitesse = (float) $res[0]["vitesse"] ;	

		$this->modele = $modele ;
		$this->attaque = $attaque ;
		$this->portee = $portee ;
		$this->blindage = $blindage ;
		$this->vitesse = $vitesse ;
		$this->vie = 100 ;
	}

	// Fonction qui recupère les statistiques du char du joueur
	public function set_char(string $modele, float $attaque, float $portee, float $blindage, float $vitesse, int $vie) {
		$this->modele = $modele ;
		$this->attaque = $attaque ;
		$this->portee = $portee ;
		$this->blindage = $blindage ;
		$this->vitesse = $vitesse ;
		$this->vie = $vie ;
	}
	
	/*
	//Affichage de la vie du vehicule 
	public function affichage_vie() {
		?> <p> Vie : <?php echo $this->vie ?> </p> <?php
	}
	*/	

	// Configure la vie du vehicule
	public function set_vie(float $vie) {
		$this->vie = $vie ;
	}

	// Renvoie la vie du vehicule
	public function get_vie() {
		return $this->vie ;
	}

	// Fonction pour réparer le vehicule
	public function reparation_vehicule(Vehicule $char) {
		$this->vie = $this->vie + 30 ;
	}

	// Fonction qui regénère la vie du vehicule
	public function reset_life() {
		$this->vie = 100 ;
	}

	// Mise à jour des stats
	public function up_grade_char(float $attaque, float $portee, float $blindage, float $vitesse) {
	}

	public function set_cible(string $modele) {
	}

	// Affichage des statistiques du char du joueur
	public function affichage_stat_char_joueur(int $id_vehicule) {
		try{
 			$pdo = new PDO('pgsql:host=localhost ; port=5432 ; user=pa ; dbname=php_war ;password=root');
		}catch (PDOException $e){
			echo $e->getMessage("erreur connection");
		}
		$niv = $pdo->prepare("SELECT niveau FROM vehicule WHERE $id_vehicule = :id_vehicule") ;
		$niv->bindParam(":id_vehicule",$id_vehicule) ;
		$niv->execute() ;
		$res = $niv->fetchAll();
		$niveau = (int) $res[0]["niveau"] ;

		?> 
		<img src="img/<?php echo $this->modele ?>.jpg" alt="<?php echo $this->modele ?>" height="150" width="240"> 
		<p> Modele : <?php echo $this->modele ?>     Niveau : <?php echo $niveau ?> </p> 
		<p> Attaque : <?php echo $this->attaque ?>     Portée : <?php echo $this->portee ?> </p> 
		<p> Blindage : <?php echo $this->blindage ?>     Vitesse : <?php echo $this->vitesse ?> </p> 
		<p> Vie : <?php echo $this->vie ?> </p> <?php
	}

	// Affichage des statistiques du char adverse
	public function affichage_stat_cible() {
		?>
		<img src="img/<?php echo $this->modele ?>.jpg" alt="<?php echo $this->modele ?>" height="150" width="240"> 
		<p> Modele : <?php echo $this->modele ?> </p> 
		<p> Attaque : <?php echo $this->attaque ?>     Portée : <?php echo $this->portee ?> </p> 
		<p> Blindage : <?php echo $this->blindage ?>     Vitesse : <?php echo $this->vitesse ?> </p> 
		<p> Vie : <?php echo $this->vie ?> </p> <?php
	}
}
?>