<h1> PHP WAR </h1> 

<?php

	/* 
	Penser a mettre de l'EXP
	Revoir les stats des chars (trop faible pour les boss)
	*/

	require 'Vehicule.php' ;
	$retour_ligne =  "<br>" ;

	session_start();
	$id_user = $_SESSION["id_user"] ;
	$identifiant = $_SESSION["identifiant"] ; 
	$id_cible = $_SESSION["id_cible"] ; 
	$combat = $_SESSION["combat"] ; // Etat du combat
	$distance = $_SESSION["distance"] ; // Distance entre les deux chars
	$vie_char = $_SESSION["vie_char"] ;  // Vie du char du joueur
	$vie_cible = $_SESSION["vie_cible"] ; // Vie du cha adverse

	try{
		$pdo = new PDO('pgsql:host=localhost ; port=5432 ; user=pa ; dbname=php_war ;password=root');
		//echo $retour_ligne . "connecté" ;
	}catch (PDOException $e){
		echo $e->getMessage("erreur connection");
	}

	// Gestion de la distance
	if ($combat == 0) {
		$distance = rand(6,10) ;
	} else {
		$distance = $_SESSION["distance"] ;
	}

	// Chargement du véhicule du joueur
	$recup_id_vehicule= $pdo->prepare("SELECT id_vehicule,modele,attaque,portee,blindage,vitesse FROM vehicule WHERE id_user = :id_user") ;
	$recup_id_vehicule->bindParam(":id_user",$id_user) ;
	$recup_id_vehicule->execute() ;
	$res=$recup_id_vehicule->fetchAll() ;
	$id_vehicule = (int) $res[0]["id_vehicule"] ;
	$modele = (string) $res[0]["modele"] ;
	$attaque = (float) $res[0]["attaque"] ;
	$portee = (float) $res[0]["portee"] ;		
	$blindage = (float) $res[0]["blindage"] ;		
	$vitesse = (float) $res[0]["vitesse"] ;

	// Initialisation du véhicule du joueur
	$char = new Vehicule($modele) ;
	$char->set_char($modele,$attaque,$portee,$blindage,$vitesse,$vie_char) ;

	// 	Test si combat en cours
	if ($combat == 0 ) {

		// Le combat commence
		$combat = 1 ; 
		$_SESSION["combat"] = $combat ;

		// Affectation d'une cible selon le niveau du joueur 
		$id_cible = rand(7,15) ;

		// Chargement du vehicule adverse 
		$recup_cible= $pdo->prepare("SELECT modele,attaque,portee,blindage,vitesse FROM char WHERE id_char = :id_cible") ;
		$recup_cible->bindParam(":id_cible",$id_cible) ;
		$recup_cible->execute() ;
		$res=$recup_cible->fetchAll() ;
		$modele_cible = (string) $res[0]["modele"] ;
		$attaque_cible = (float) $res[0]["attaque"] ;
		$portee_cible = (float) $res[0]["portee"] ;		
		$blindage_cible = (float) $res[0]["blindage"] ;		
		$vitesse_cible = (float) $res[0]["vitesse"] ;

		// Initialisation du véhicule adverse
		$cible = new Vehicule($modele_cible) ;
		$cible->set_vie($vie_cible) ;

		// Sauvegarde de l'adversaire en session
		$_SESSION["id_cible"] = $id_cible ;
		
	} else {

		// Récupération de l'adversaire
		$id_cible = $_SESSION["id_cible"] ;
		$recup_cible= $pdo->prepare("SELECT modele,attaque,portee,blindage,vitesse FROM char WHERE id_char = :id_cible") ;
		$recup_cible->bindParam(":id_cible",$id_cible) ;
		$recup_cible->execute() ;
		$res=$recup_cible->fetchAll() ;
		$modele_cible = (string) $res[0]["modele"] ;
		$attaque_cible = (float) $res[0]["attaque"] ;
		$portee_cible = (float) $res[0]["portee"] ;		
		$blindage_cible = (float) $res[0]["blindage"] ;		
		$vitesse_cible = (float) $res[0]["vitesse"] ;
		$cible = new Vehicule($modele_cible) ;
		$vie_cible = $_SESSION["vie_cible"] ;
		$cible->set_vie($vie_cible) ;
	}

	// Attaque du joueur 
	if(isset($_POST['attaquer'])) {

		// Si le joueur est à portée
		if ($distance <= $portee) {
			$vie_cible = $vie_cible - (($attaque_cible * 20) - ($blindage * 5 + $vitesse * 2)) ;
			$cible->set_vie($vie_cible) ;
	//		$_SESSION["vie_cible"] = $vie_cible ;
			$_SESSION["message"] = "Vous avez touché votre cible !" ; 
			$_SESSION["message_cible"] = "" ;
		} else {
			$_SESSION["message"] = "Vous n'êtes pas à portée !" ; 
			$_SESSION["message_cible"] = "" ;
		}

		// Si l'adversaire est détruit
		if ($vie_cible <= 0) {
			$combat = 0 ;
			$vie_cible = 100 ;
			$_SESSION["combat"] = $combat ;
			$_SESSION["vie_cible"] = $vie_cible ;
			$_SESSION["message"] = "Bravo, cible détruite !" ;
			$_SESSION["message_cible"] = "Nouvelle cible en approche !" ;
			header('Location: http://pwcs.local/jeu.php');
			exit();
		}

		// Si l'adversaire est à portée, il tire
		if ($distance <= $portee_cible) {
			$vie_char = $vie_char - (($attaque_cible * 20) - ($blindage * 5 + $vitesse * 2)) ;
			$char->set_vie($vie_char) ;
			$_SESSION["message_cible"] = "Vous êtes touché !" ;
		// Sinon il avance
		} else if ($distance > $portee_cible && $distance-$vitesse_cible > 0 ) {
			$distance = $distance - $vitesse_cible ; 
			$_SESSION["distance"] = $distance ;
			$_SESSION["message_cible"] = "L'ennemi avance !" ;
		} else if ($distance = $vitesse_cible ) {
			$distance = $distance - $vitesse_cible + 1; 
			$_SESSION["message_cible"] = "L'ennemi se met en position de tir !" ;
		}

		// Si le joueur n'a plus de vie, il perd
		if ($vie_char<=0) {
			$_SESSION["message"] = "Votre char a été détruit" ;
			header('Location: http://pwcs.local/game_over.php');
			exit();
		}
	}
	$_SESSION["distance"] = $distance ;
	$_SESSION["vie_char"] = $vie_char ;
	$_SESSION["vie_cible"] = $vie_cible ;
	

	// Avancée du joueur 
	if(isset($_POST['avancer'])) { 

		// Si le joueur et l'adversaire peuvent avancer
		if (($distance-$vitesse)>0) {
			$distance = $distance-$vitesse ;
			$_SESSION["message"] = "Vous vous repprochez de votre cible $identifiant !" ; 
		} else {
			$distance = 1 ;
			$_SESSION["message"] = "Vous ne pouvez plus avancer !" ; 
			$_SESSION["message_cible"] = "" ;
		}

		// Si l'adversaire est portée il tire, sinon il avance 
		if ($distance <= $portee_cible) {
			$vie_char = $vie_char - (($attaque_cible * 20) - ($blindage * 5 + $vitesse * 2)) ;
			$char->set_vie($vie_char) ;
			$_SESSION["vie_char"] = $vie_char ;
			$_SESSION["message_cible"] = "Vous êtes touché $identifiant !" ;
		} else if (($distance-$vitesse_cible)>0) {
			$distance = $distance-$vitesse_cible ;
			$_SESSION["message_cible"] = "L'ennemi avance ! " ;
		}
	}
	$_SESSION["distance"] = $distance ;
	

	// Le joueur passe son tour
	if(isset($_POST['stationnement'])) { 

		// Si l'adversaire peut avancer il avance
		if ($distance-$vitesse_cible>0) {
			$distance = $distance-$vitesse_cible ; 
			$_SESSION["message"] = "" ;
			$_SESSION["message_cible"] = "L'ennemi avance !" ;
		// Sinon il tire s'il est à portée
		} else if ($distance <= $portee_cible) {
			$vie_char = $vie_char - (($attaque_cible * 20) - ($blindage * 5 + $vitesse * 2)) ;
			$char->set_vie($vie_char) ;
			$_SESSION["vie_char"] = $vie_char ;
			$_SESSION["message"] = "" ;
			$_SESSION["message_cible"] = "Vous êtes touché !" ;
		}

		// Si le joueur n'a plus de vie, il perd
		if ($vie_char<=0) {
			$_SESSION["message"] = "Votre char a été détruit" ;
			$_SESSION["message_cible"] = "" ;
			header('Location: http://pwcs.local/game_over.php');
			exit();
		}
	}
	$_SESSION["distance"] = $distance ;
	

	// Répération du char du joueur
	if(isset($_POST['reparation'])) { 
		$vie_char = $char->get_vie() ;
		// Le joueur peut faire une réparation s'il le peut
		if ($vie_char<100) {
			$char->reparation_vehicule($char) ;
			$vie_char = $char->get_vie() ;
			if ($vie_char>100) {
				$vie_char = 100 ;
			}

			$_SESSION["message"] = "Réparation effectuée !" ;
		} else {
			$_SESSION["message"] = "Réparation impossible !" ;
			$vie_char = $char->get_vie() ;
			if ($vie_char>100) {
				$vie_char = 100 ;
			}
		}

		// Si l'adversaire peut avancer, il avance
		if ($distance <= $portee_cible) {
			$vie_char = $vie_char - (($attaque_cible * 20) - ($blindage * 5 + $vitesse * 2)) ;
			$char->set_vie($vie_char) ;
			$_SESSION["vie_char"] = $vie_char ;
			$_SESSION["message_cible"] = "Vous êtes touché !" ;
		// Sinon il tire s'il est à portée
		} else if ($distance-$vitesse_cible>0) {
			$distance = $distance-$vitesse_cible ; 
			$_SESSION["message_cible"] = "L'ennemi avance !" ;
		} else if ($distance = $portee_cible) {
			$_SESSION["message_cible"] = "L'ennemi se met en position de tir !" ;
		}

		// Si le joueur n'a plus de vie, il perd
		if ($vie_char<=0) {
			$_SESSION["message"] = "Votre char a été détruit" ;
			header('Location: http://pwcs.local/game_over.php');
			exit();
		}
	}
	$_SESSION["distance"] = $distance ;
	$_SESSION["vie_char"] = $vie_char ;

	// Fuite du joueur
	if(isset($_POST['replie'])) {
		$vie_char = $char->get_vie() ;
		$_SESSION["vie_char"] = $vie_char ;
		$_SESSION["vie_cible"] = 100 ;
		$_SESSION["combat"] = 0 ;
		$_SESSION["message"] = "Vous vous êtes replié, vous êtes un lâche !" ;
		$_SESSION["message_cible"] = "" ;
		header('Location: http://pwcs.local/jeu.php');
		exit();
	}

	// Le joueur quitte le jeu
	if(isset($_POST['quitter'])) { 
		session_destroy(); 
		header('Location: http://pwcs.local');
		exit();
	}

	// Check vie  
	$vie_char = $char->get_vie();
	if ($vie_char>=100) { 
		$char->set_vie(100) ;
		$_SESSION["vie_char"] = $vie_char ;
	}
	
	/**********************/
	/*     AFFICHAGE      */
	/**********************/

	
	$message = $_SESSION["message"] ;
	if ($message != "" ) {
		echo $message ;
		echo $retour_ligne ;
	}
	$message = $_SESSION["message_cible"] ;
	if ($message != "" ) {
		echo $message ;
		echo $retour_ligne ;
	}
	// Affichage char du joueur
	$char->affichage_stat_char_joueur($id_vehicule) ;

	// Affichage distance 
	echo " Distance : " . $distance ;
	echo $retour_ligne ;
	echo $retour_ligne ;

	// Affichage des actions possibles par le joueur 
	?>
 	<form action="#" method="post">
 		<input type="submit" name="attaquer" value="Attaquer" value="true">
 		<input type="submit" name="avancer" value="Avancer" value="true">
 		<input type="submit" name="stationnement" value="Stationnement" value="true">
 	</form> 
 	<form action="#" method="post">
 		<input type="submit" name="reparation" value="Réparation" value="true">
 		<input type="submit" name="replie" value="Se replier" value="true">
 		<input type="submit" name="quitter" value="Quitter" value="true">
	</form>  
	<?php

	// Affichage de la cible
	$cible->affichage_stat_cible() ;

?>

