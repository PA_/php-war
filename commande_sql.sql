/* Création de la base de données */
CREATE DATABASE php_war ;

/* Création des tables*/
CREATE TABLE utilisateur(id_user SERIAL NOT NULL PRIMARY KEY UNIQUE, pseudo VARCHAR(30) UNIQUE, password VARCHAR(60)) ;

CREATE TABLE vehicule(id_vehicule SERIAL NOT NULL PRIMARY KEY UNIQUE, id_user  INT NOT NULL REFERENCES utilisateur (id_user), 
type VARCHAR(6) NOT NULL, modele VARCHAR(30), niveau INT NOT NULL DEFAULT 1, attaque REAL NOT NULL, portee REAL NOT NULL, blindage REAL NOT NULL, 
vitesse REAL NOT NULL, victoire INT NOT NULL DEFAULT 0, defaite INT NOT NULL DEFAULT 0) ;

CREATE TABLE char(id_char SERIAL NOT NULL PRIMARY KEY UNIQUE, modele VARCHAR(30), rang INT NOT NULL, attaque REAL NOT NULL, 
portee REAL NOT NULL, blindage REAL NOT NULL, vitesse REAL NOT NULL) ;

/* Remplissage table char */ 
/* Chars principals itilisable par l'utilisateur */
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('M1-Abrams',5,4,2,2,2) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Leclerc',5,2,3,2,3) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Challenger_2',5,2,3,4,1) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('T-90',5,3,2,1,4) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Merkava',5,3,4,2,1) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Leopard_2',5,3,4,2,1) ;

/* Chars secondaires de LVL 1 (5) servant de cible pour l'utilisateur */ 
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('T-34',1,1,1,1,2) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Panzer_4',1,2,1,1,1) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Sherman',1,1,2,1,1) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Somua',1,1,1,2,1) ;

/* Chars secondaires de LVL 2 (6) servant de cible pour l'utilisateur */ 
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Tigre_1',2,2,2,1,1) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('AMX-30',2,2,1,1,2) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('T-62',2,1,1,2,2) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Centurion',2,1,2,2,1) ;

/* Chars secondaires de LVL 3 (8) servant de cible pour l'utilisateur */ 
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Leopard_1',3,2,2,2,2) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('T-72',3,2,2,1,3) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Stridsvagn_103',3,2,3,2,1) ;

/* Chars secondaires de LVL 4 (9) servant de cible pour l'utilisateur */ 
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('T-80',4,3,3,1,3) ;
INSERT INTO char(modele,rang,attaque,portee,blindage,vitesse) VALUES('Type-96',4,2,2,3,1) ;

/*
DROP TABLE utilisateur CASCADE ;
DROP TABLE vehicule CASCADE ;
DROP TABLE char CASCADE ;
*/