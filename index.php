<?php 

	// Controle pour tester si l'on est connecté avec la base de données
	try{
 		$pdo = new PDO('pgsql:host=localhost ; port=5432 ; user=pa ; dbname=php_war ;password=root');
 		//echo "Entrée dans la base" ;
	}catch (PDOException $e){
		echo $e->getMessage("erreur connection");
	}

	// Formulaire de connexion ?>
	<h1> AEIC War </h1> 
	<form action="index.php" method="post">
	    <p> Identifiant/pseudo : <input type="text" name="identifiant" value="" />  </p>
	    <p> Mot de passe : <input type="password" name="password" value="" />  </p>
	    <input type="submit" name="connexion" value="Connexion" />
	    <input type="submit" name="inscription" value="Inscription" />
	</form>

<?php 

	// Si on veut se connecter 
	if(isset($_POST['connexion'])) { 
		$identifiant = ($_POST['identifiant']) ;
		$password = ($_POST['password']) ;

		// Recherche de l'utilisateur dans la base de donnée
	    $connexion= $pdo->prepare("SELECT count(*) FROM utilisateur WHERE pseudo = :identifiant AND password = :password ");
	    $connexion->bindParam(":identifiant",$identifiant) ;
	    $password = hash('ripemd160',"$password") ;
	    $connexion->bindParam(":password",$password) ;
	    $connexion->execute() ;
	    $resultat = $connexion->fetchColumn() ;

	    // Test pour savoir si l'utilisateur existe
	    if($resultat == 0) {
	        echo "Identifiant et/ou mot de passe incorrect";
	    } else {
	    	// Récupération de l'id_user dans la table utilisateur
			$recup_user=$pdo->prepare("SELECT id_user FROM utilisateur WHERE pseudo LIKE :user") ;
			$recup_user->bindParam(":user",$identifiant) ;
			$recup_user->execute() ;
			$id_user = $recup_user->fetchColumn();

			// Stockage des paramètres de session
	    	session_start();
	    	$_SESSION["id_user"] = $id_user ;
	    	$_SESSION["identifiant"] = $identifiant ;
	    	$_SESSION["id_cible"] = 0 ;
	    	$_SESSION["combat"] = 0 ;
	    	$_SESSION["distance"] = 1 ;
	    	$_SESSION["modele"] = "" ;
	    	$_SESSION["modele_cible"] = "" ;
	    	$_SESSION["attaque_cible"] = 0 ;
	    	$_SESSION["portee_cible"] = 0 ;
	    	$_SESSION["blindage_cible"] = 0 ;
	    	$_SESSION["portee_cible"] = 0 ;
	    	$_SESSION["vitesse_cible"] = 0 ;
	    	$_SESSION["vie_char"] = 100 ;
	    	$_SESSION["vie_cible"] = 100 ;
	    	$_SESSION["degat"] = 0 ;
	    	$_SESSION["message"] = "Bonne chance $identifiant !" ;
	    	$_SESSION["message_cible"] = "" ;

	        header('Location: http://pwcs.local/jeu.php');
			exit();
	    }
	}

	// Si on veut s'inscrire
	if (isset($_POST['inscription'])) {
	    header('Location: http://pwcs.local/inscription.php');
		exit(); 
	}
?>